///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 09a - Cat Empire!
///
/// @file cat.cpp
/// @version 1.0
///
/// Exports data about all cats
///
/// @author eduardo kho jr <eduardok@hawaii.edu>
/// @brief  Lab 09a - Cat Empire! - EE 205 - Spr 2021
/// @date   4/26/2021
///////////////////////////////////////////////////////////////////////////////

#include <cassert>
#include <random>
#include <queue>

#define CAT_NAMES_FILE "names.txt"

#include "cat.hpp"

using namespace std;

Cat::Cat( const std::string newName ) {
	setName( newName );
}


void Cat::setName( const std::string newName ) {
	assert( newName.length() > 0 );

	name = newName;
}


std::vector<std::string> Cat::names;


void Cat::initNames() {
	names.clear();
	cout << "Loading... ";

	ifstream file( CAT_NAMES_FILE );
	string line;
	while (getline(file, line)) names.push_back(line);

	cout << to_string( names.size() ) << " names." << endl;
}


Cat* Cat::makeCat() {
	// Get a random cat from names
	auto nameIndex = rand() % names.size();
	auto name = names[nameIndex];

	// Remove the cat (by index) from names
	erase( names, names[nameIndex] );

	return new Cat( name );
}


void CatEmpire::catFamilyTree() const {
	if( topCat == nullptr ) {
		cout << "No cats!" << endl;
		return;
	}

	dfsInorderReverse( topCat, 1 );
}

void CatEmpire::dfsInorderReverse( Cat* atCat, int depth ) const { // Right, Parent, Left //
	if( atCat == nullptr){
	return;
	}
	
	CatEmpire::dfsInorderReverse( atCat->right, depth + 1);		
	cout << string( 6 * (depth-1), ' ' ) << atCat -> name;
	if( (atCat -> left != nullptr) && (atCat -> right != nullptr) ){
	cout << "<" << endl;
	}
	if( (atCat -> left != nullptr) && (atCat -> right == nullptr)){
	cout << "\\" << endl;
	}
	if( (atCat -> right != nullptr) && (atCat -> left == nullptr)){
	cout << "/ " << endl;
	}
	if( (atCat -> left == nullptr) && (atCat -> right == nullptr) ){
	cout<<endl;
	}
	cout<<endl;	
	CatEmpire::dfsInorderReverse( atCat->left, depth + 1 );
	}

void CatEmpire::catList() const {
	
	if( topCat == nullptr ) {
		cout << "No cats!" << endl;
		return;
	}
	
	dfsInorder( topCat ); // Left, Parent, Right //
}

void CatEmpire::dfsInorder( Cat* atCat ) const{
	if( atCat == nullptr ){
	return;
	}
	CatEmpire::dfsInorder( atCat->left );
	cout << string(0,' ')<< atCat -> name << endl;
	CatEmpire::dfsInorder( atCat->right );
}

void CatEmpire::catBegat() const {
	if( topCat == nullptr ) {
		cout << "No cats!" << endl;
		return;
	}

	dfsPreorder( topCat ); // Parent, Left, Right
}

void CatEmpire::dfsPreorder( Cat* atCat ) const{
	if( atCat == nullptr ){
	return;
	}
	if( (atCat->left !=nullptr) && (atCat->right !=nullptr) ){
	cout << atCat->name << " begat " << atCat->left->name << " and " << atCat->right->name <<endl;
	}
	if( (atCat->left == nullptr) && (atCat->right == nullptr) ){
	return;
	}
	if( (atCat->left == nullptr) && (atCat->right != nullptr) ){
	cout<< atCat -> name << " begat " << atCat->right->name << endl;
	}
	if( (atCat->right == nullptr) && (atCat->left != nullptr) ){
	cout<< atCat -> name << " begat " << atCat->left->name << endl;
	}
	CatEmpire::dfsPreorder( atCat->left );
	CatEmpire::dfsPreorder( atCat->right );
}

bool CatEmpire::empty(){
	if(topCat == nullptr){
		return true;
	}
	return false;
}

void CatEmpire::addCat( Cat* atCat, Cat* newCat ){
	if( atCat -> name > newCat -> name ) {
		if( atCat -> left == nullptr ){
			atCat -> left = newCat;
		}else{
			addCat( atCat -> left, newCat);
		}
	}
	if( atCat -> name < newCat -> name){
		if( atCat -> right == nullptr){
			atCat -> right = newCat;
		}else{
			addCat( atCat -> right, newCat);
		}
	}
}


void CatEmpire::addCat( Cat* newCat ){
 	if ( topCat == nullptr ){
		topCat = newCat;
		return;
	}
	
	addCat( topCat, newCat );
}

void CatEmpire::catGenerations() const{
	if( topCat == nullptr ){
	cout<< "no cats!"<<endl;
	}	
	
	CatEmpire::bfs(topCat);

}

void CatEmpire::getEnglishSuffix( int n ) const{
	if(n%100 == 11 || n%100==12 || n%100 ==13){
		cout << n << "th Generation"<<endl;
		cout << " ";
	}else{
		if(n%10 == 1){
      			cout << n << "st Generation" << endl;
			cout << " ";
    		}else{
      			if(n%10 == 2){
          			cout << n << "nd Generation" << endl;
				cout << " ";
       		 	}else{
          			if(n%10 == 3){
          				cout << n << "rd Generation" << endl;
					cout << " ";
        			}else{
          				if(n%10 == 4 || n%10 == 5 || n%10 == 6 || n%10 == 7 || n%10 == 8 || n%10 == 9 || n%10 == 0){
            					cout << n << "th Generation" << endl;
						cout << " ";
        				}
        			}
        		}
    		}
    	}		
}
	


void CatEmpire::bfs( Cat* atCat ) const{

	int generation = 1;
	queue<Cat*> catQueue;
	catQueue.push(topCat);
	while( !catQueue.empty() ){
		CatEmpire::getEnglishSuffix( generation );
		long unsigned int gen_size = catQueue.size();
		while(gen_size!=0){
			Cat* cat = catQueue.front();
			cout << cat->name;
			catQueue.pop();	
			if(cat->left!=nullptr){
				catQueue.push(cat->left);
			}
			if(cat->right!=nullptr){
				catQueue.push(cat->right);
			}	
		gen_size--;
		cout<< "  ";
		}
	cout<<endl;	
	generation++;
	}	
}
